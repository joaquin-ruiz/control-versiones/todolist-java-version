package com.example.vicky.todolist;

public class Const {
    // _DATABASE VARIABLES
    public static final String _DB_NAME = "ToDoList";
    public static final int _DB_VERSION = 1;
    public static final String _TABLE_TODO = "ToDo";
    public static final String _COLD_ID = "id";
    public static final String _COL_CREATED_AT = "createdAt";
    public static final String _COL_NAME = "name";
    public static final String _TABLE_TODO_ITEM = "ToDoItem";
    public static final String _COL_TODO_ID = "toDoId";
    public static final String _COL_ITEM_NAME = "itemName";
    public static final String _COL_IS_COMPLETED = "isCompleted";
    public static final String _INTENT_TODO_ID = "TodoId";
    public static final String _INTENT_TODO_NAME = "TodoName";

}
