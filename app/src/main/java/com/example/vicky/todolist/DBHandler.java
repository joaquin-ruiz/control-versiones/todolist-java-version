package com.example.vicky.todolist;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.vicky.todolist.DTO.ToDo;
import com.example.vicky.todolist.DTO.ToDoItem;

import java.util.ArrayList;

import static com.example.vicky.todolist.Const.*;

public class DBHandler extends SQLiteOpenHelper {


    public DBHandler(Context context) {
        super(context, _DB_NAME, null, _DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createToDoTable = "CREATE TABLE " + _TABLE_TODO + " (" +
                _COLD_ID + " integer PRIMARY KEY AUTOINCREMENT," +
                _COL_CREATED_AT + " datetime DEFAULT CURRENT_TIMESTAMP," +
                _COL_NAME + " varchar)";
        String createToDoItemTable =
                "CREATE TABLE " + _TABLE_TODO_ITEM + " (" +
                        _COLD_ID + " integer PRIMARY KEY AUTOINCREMENT," +
                        _COL_CREATED_AT + " datetime DEFAULT CURRENT_TIMESTAMP," +
                        _COL_TODO_ID + " integer," +
                        _COL_ITEM_NAME + " varchar," +
                        _COL_IS_COMPLETED + " integer)";

        db.execSQL(createToDoTable);
        db.execSQL(createToDoItemTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    boolean addToDo(ToDo todo) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(_COL_NAME, todo.getName());
        long result = db.insert(_TABLE_TODO, null, cv);
        return result != -1;
    }


    void updateToDo(ToDo todo) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(_COL_NAME, todo.getName());
        db.update(_TABLE_TODO, cv, _COLD_ID + "=?", new String[]{String.valueOf(todo.getId())});
    }

    void deleteToDo(Long todoId) {
        SQLiteDatabase db = getWritableDatabase();
        // Añadir comprobacion para la subtarea.
        db.delete(_TABLE_TODO_ITEM, _COL_TODO_ID + "=?", new String[]{String.valueOf(todoId)});
        db.delete(_TABLE_TODO, _COLD_ID + "=?", new String[]{String.valueOf(todoId)});
    }

    void updateToDoItemCompletedStatus(Long todoId, Boolean isCompleted) {
        SQLiteDatabase db = getWritableDatabase();
        Cursor queryResult = db.rawQuery("SELECT * FROM " + _TABLE_TODO_ITEM + " WHERE " + _COL_TODO_ID + "=" + todoId, null);
        if (queryResult.moveToFirst()) {
            do {
                ToDoItem item = new ToDoItem();
                item.setId(queryResult.getLong(queryResult.getColumnIndex(_COLD_ID)));
                item.setToDoId(queryResult.getLong(queryResult.getColumnIndex(_COL_TODO_ID)));
                item.setItemName(queryResult.getString(queryResult.getColumnIndex(_COL_ITEM_NAME)));
                item.setCompleted(isCompleted);
                updateToDoItem(item);
            } while (queryResult.moveToNext());
        }
        queryResult.close();
    }

    public void updateToDoItem(ToDoItem item) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(_COL_ITEM_NAME, item.getItemName());
        cv.put(_COL_TODO_ID, item.getToDoId());
        cv.put(_COL_IS_COMPLETED, item.isCompleted());
        db.update(_TABLE_TODO_ITEM, cv, _COLD_ID + "=?", new String[]{String.valueOf(item.getId())});
    }

    ArrayList<ToDo> getToDos() {
        ArrayList<ToDo> result = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor queryResult = db.rawQuery("SELECT * from " + _TABLE_TODO, null);
        if (queryResult.moveToFirst()) {
            do {
                ToDo todo = new ToDo();
                todo.setId(queryResult.getLong(queryResult.getColumnIndex(_COLD_ID)));
                todo.setName(queryResult.getString(queryResult.getColumnIndex(_COL_NAME)));
                result.add(todo);
            } while (queryResult.moveToNext());
        }
        queryResult.close();
        return result;
    }

    public boolean addToDoItem(ToDoItem item) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(_COL_ITEM_NAME, item.getItemName());
        cv.put(_COL_TODO_ID, item.getToDoId());
        cv.put(_COL_IS_COMPLETED, item.isCompleted());

        long result = db.insert(_TABLE_TODO_ITEM, null, cv);
        return result != -1;
    }

    public void deleteToDoItem(long itemId) {
        SQLiteDatabase db = getWritableDatabase();
        Log.d("id subtarea:",  String.valueOf(itemId));
        db.delete(_TABLE_TODO_ITEM, _COLD_ID + "=?", new String[]{String.valueOf(itemId)});

    }

    public ArrayList<ToDoItem> getToDoItems(long todoId) {
        ArrayList<ToDoItem> result = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor queryResult = db.rawQuery("SELECT * FROM " + _TABLE_TODO_ITEM + " WHERE " + _COL_TODO_ID + "=" + todoId, null);
        if (queryResult.moveToFirst()) {
            do {
                ToDoItem item = new ToDoItem();
                item.setId(queryResult.getLong(queryResult.getColumnIndex(_COLD_ID)));
                item.setToDoId(queryResult.getLong(queryResult.getColumnIndex(_COL_TODO_ID)));
                item.setItemName(queryResult.getString(queryResult.getColumnIndex(_COL_ITEM_NAME)));
                item.setCompleted(queryResult.getInt(queryResult.getColumnIndex(_COL_IS_COMPLETED)) == 1);
                result.add(item);
            } while (queryResult.moveToNext());
        }

        queryResult.close();
        return result;
    }
}
